package com.rest_kafka.kafka_rest_project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaRestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaRestProjectApplication.class, args);
	}

}
